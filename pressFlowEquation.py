# Script for pressure and flow rate calculation

import math

def pressFlowEquation(t, z, fa, k, ms, poco_radius, pres):
		p = z[0]
		q = z[1]
		dpdx = (32*fa*ms*(q**2))/((math.pi**2)*((2*poco_radius)**5))
		dqdx = -k*(p - pres)
		dzdx = [dpdx, dqdx]
		return dzdx