import pandas as pd
import numpy as np
import os
#from keras.models import Sequential
#from keras.layers import Dense
#from ann_visualizer.visualize import ann_viz
#from keras import optimizers
import matplotlib.pyplot as plt
import statistics
import math
import random

#column_names = ['MD', 'K', 'x', 'Kad', 'net_input', 'coef']
#columns_length = [345, 345, 345, 345, 6, 7]
#net_input = [Bres, Hres, p_poco, poco_radius, mi, ms]

path = '/home/rodrigo/Downloads/Dados_Simulacoes_ip3d_rede_neurais_old'
poco_len = 400
kh = 1500
kv = 450
km = (kh + kv + 0)/3

k_file_name = 'simML020_117_800_006_400_15_K.csv'
r_file_name = k_file_name.split('K.csv')[0] + 'R.csv'
p_file_name = k_file_name.split('K.csv')[0] + 'P.csv'
q_file_name = k_file_name.split('K.csv')[0] + 'Q.csv'

ms = float(k_file_name.split('_')[2])
mi = float(k_file_name.split('_')[3])/1000

K_df = pd.read_csv(k_file_name, encoding='utf-8', sep=',')
R_df = pd.read_csv(r_file_name, encoding='latin1', sep='\t', skiprows=1)
P_df = pd.read_csv(p_file_name, encoding='latin1', sep='\t', skiprows=1)
Q_df = pd.read_csv(q_file_name, encoding='latin1', sep='\t', skiprows=1)

md_to_m2 = 1e-15
pressure_to_pascal = 98066
pres = 225*pressure_to_pascal

n = 345
h = -1
t = np.flip(np.arange(n),0)

q_orig_interp = np.zeros( shape=(n) )
p_md = P_df['MD (m)'].values
q_md = Q_df['MD (m)'].values
q_orig = Q_df.iloc[:,1].values
p_orig = P_df.iloc[:,1].values*pressure_to_pascal
for i in range(0,n-1):
    mdi = p_md[i]
    diff = np.absolute(q_md - mdi)
    idx = np.argmin(diff)
    q_orig_interp[i] = q_orig[idx]

#p = np.empty_like(t)
#q = np.empty_like(t)

p = np.zeros( shape=(len(t)) )
q = np.zeros( shape=(len(t)) )

#pheel = float(k_file_name.split('_')[1])
#pheel = pheel*pressure_to_pascal
pheel = p_orig[0]

deltap = random.uniform(1, 10)*pressure_to_pascal

ptoe = pres - deltap

p[0] = ptoe
RE = R_df.iloc[:,1].values
deltap_inc = 1.01
deltap_dec = 0.99

D = 0.2
rho = 900

#kad = K_df.iloc['Kad'].values

k = K_df.iloc[:,1].values

k_inv = np.flip(k, 0)

tol = 0.001*pressure_to_pascal
num_iter = 1000
iter_count = 0
best_iter = 0

md = K_df['MD'].values

md_r = R_df.iloc[:,0].values

md_inv = np.flip(md,0)

RE_orig_interp = np.zeros( shape=(n) )
for i in range(0,n-1):
    mdi = p_md[i]
    diff = np.absolute(md_r - mdi)
    idx = np.argmin(diff)
    RE_orig_interp[i] = RE[idx]

def model(t, z, fa, k):
    p = z[0]
    q = z[1]
    dpdx = (32*fa*rho*(q**2))/((math.pi**2)*(D**5))
    dqdx = -k*(p - pres)
    dzdx = [dpdx, dqdx]
    return dzdx

errors = []
best_p = []
best_q = []
while iter_count < num_iter:
    iter_count = iter_count + 1
    error = abs(p[n-1] - pheel)

    #print('p[344]: ' + str(p[n-1]))
    #print('error: ' + str(error))

    if not errors:
        best_p = p
        best_q = q
    else:
        if min(errors) > error:
            #print('update best result')
            best_p = p
            best_q = q
            best_iter = iter_count
            #print('best_iter: ' + str(best_iter))
    
    errors.append(error)
    #print('errors:' + str(errors))

    if error < tol:
        break
    else:
        if p[n-1] > pheel:
            deltap = deltap*deltap_inc
            p[0] = pres - deltap
        else:
            deltap = deltap*deltap_dec
            p[0] = pres - deltap

    #print('deltap: ' + str(deltap))
    #print('p[0]: ' + str(p[0]))

    REs = np.zeros( shape=(len(t)) )

    for i in range(0,len(t)-1):
        #md_val = md_inv[i]
        #diff = np.absolute(md_r - md_val)
        #idx = np.argmin(diff)
        #REi = RE[idx]
        
        #REi = (ms*((2/(math.pi*(D/2)))*q_orig_interp[i]))/mi
        REi = 0
        fa = 0
        if q[i] != 0:
            REi = (ms*((2/(math.pi*(D/2)))*abs(q[i])))/mi
            fa = 0.0791/(REi**0.25)
        #if REi == 0:
        #    REi = 2100.0

        REs[i] = REi

        m0 = model(t[i], [p[i], q[i]], fa, k_inv[i])
        m1 = model(t[i]+0.5*h, [p[i]+0.5*h*m0[0], q[i]+0.5*h*m0[1]], fa, k_inv[i])
        m2 = model((t[i]+0.5*h), [p[i]+0.5*h*m1[0], q[i]+0.5*h*m1[1]], fa, k_inv[i])
        m3 = model(t[i]+h, [p[i]+h*m2[0], q[i]+h*m2[1]], fa, k_inv[i])
        p[i+1] = p[i] + (1/6)*( m0[0]+2*m1[0]+2*m2[0]+m3[0] )*h
        q[i+1] = q[i] + (1/6)*( m0[1]+2*m1[1]+2*m2[1]+m3[1] )*h
        #print('i: ' + str(i))

p_poco = np.flip(best_p,0)
q_poco = np.flip(best_q,0)

x = np.arange(len(errors))

plt.figure()
plt.plot(p_md,p_poco,'r-',label='p(x) 1d')
plt.plot(p_md,p_orig,'b-',label='p(x) 3d')
plt.title('Comparação pressão para caso: ' + k_file_name)
plt.xlabel('md')
plt.ylabel('Pa')
plt.legend()
plt.grid()
plt.figure()
plt.plot(p_md,q_poco,'r-',label='q(x) 1d')
plt.plot(p_md,q_orig_interp,'b-',label='q(x) 3d')
plt.title('Comparação vazão para caso: ' + k_file_name)
plt.xlabel('md')
plt.ylabel('m3/s')
plt.legend()
plt.grid()
plt.figure()
plt.plot(x, np.asarray(errors), 'g-', label='e(x)')
plt.title('Erro para caso: ' + k_file_name)
plt.grid()
plt.figure()
plt.plot(p_md, np.flip(REs,0), 'r-', label='RE(x) 1d')
plt.plot(p_md, RE_orig_interp, 'b-', label='RE(x) 3d')
plt.title('Reynolds ao longo do poço')
plt.grid()
plt.legend()
#print('errors numpy: ' + str(np.asarray(errors)))

#print(iter_count)
#print(best_iter)

plt.show()
