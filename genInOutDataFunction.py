# Script function for generate net input/output

import pandas as pd
import numpy as np
import os
import math
import re
import shutil
from scipy.interpolate import CubicSpline

def genInOutDataFunction(path, pressure_to_pascal, pres, poco_len, km, input_len, output_len, pol_order):
    num_cases = 0

    net_input_mat = np.zeros(shape=(1, input_len))
    net_output_mat = np.zeros(shape=(1, output_len))

    dq_val_arr = np.zeros(shape=(1, 15))
    dq_md_arr = np.zeros(shape=(1, 15))

    q_val_arr = np.zeros(shape=(1, 32))
    q_md_arr = np.zeros(shape=(1, 32))

    p_val_arr = np.zeros(shape=(1, 345))
    p_md_arr = np.zeros(shape=(1, 345))    

    input_file_names = []

    for filename in os.listdir(path):
        if filename.endswith('P.xls'):
            num_cases = num_cases + 1
            p_filename_xls = filename
            p_filename_csv = filename.split('P.xls')[0] + 'P.csv'  # Monta os nomes dos arquivos csv a serem salvos com os dados para treinamento
            dq_filename_xls = filename.split('P.xls')[0] + 'dQ.xls'
            dq_filename_csv = filename.split('P.xls')[0] + 'dQ.csv'
            k_filename = filename.split('P.xls')[0] + 'K.csv'
            r_filename_xls = filename.split('P.xls')[0] + 'R.xls'
            r_filename_csv = filename.split('P.xls')[0] + 'R.csv'
            q_filename_xls = filename.split('P.xls')[0] + 'Q.xls'
            q_filename_csv = filename.split('P.xls')[0] + 'Q.csv'

            input_file_names.append(p_filename_csv)

            mi = float('0.' + p_filename_csv.split('_')[3])
            Bres = float(p_filename_csv.split('_')[4])
            Hres = float(p_filename_csv.split('_')[5])
            p_poco = float(p_filename_csv.split('_')[1]) * pressure_to_pascal
            #poco_radius = (float(re.findall('\d+', p_filename_csv)[0]) / 100) / 2
            poco_radius = float('0.' + re.findall('\d+', p_filename_csv)[0][1:])/2
            ms = float(p_filename_csv.split('_')[2])

            net_input_mat = np.append(net_input_mat, [[Bres, Hres, p_poco, poco_radius, mi, ms]], axis=0)

            shutil.copy(p_filename_xls, p_filename_csv)
            shutil.copy(dq_filename_xls, dq_filename_csv)
            shutil.copy(r_filename_xls, r_filename_csv)
            shutil.copy(q_filename_xls, q_filename_csv)

            print(p_filename_xls)

            pressure_df = pd.read_csv(p_filename_csv, encoding='latin1', sep='\t', skiprows=1)
            p_md = pressure_df['MD (m)'].values
            p_val = pressure_df.iloc[:,1].values*pressure_to_pascal
            dq_df = pd.read_csv(dq_filename_csv, encoding='latin1', sep='\t', skiprows=1)
            dq_md = dq_df.iloc[:,3].values[0:15].astype(float)
            dqs = dq_df.iloc[:,4].values[0:15].astype(float)

            q_df = pd.read_csv(q_filename_csv, encoding='latin1', sep='\t', skiprows=1)
            q_md = q_df.iloc[:,0].values
            qs = q_df.iloc[:,1].values

            dq_val_arr = np.append(dq_val_arr, dqs.reshape(1, 15), axis=0)
            dq_md_arr = np.append(dq_md_arr, dq_md.reshape(1, 15), axis=0)

            q_val_arr = np.append(q_val_arr, qs.reshape(1, 32), axis=0)
            q_md_arr = np.append(q_md_arr, q_md.reshape(1, 32), axis=0) 

            p_val_arr = np.append(p_val_arr, p_val.reshape(1, 345), axis=0)
            p_md_arr = np.append(p_md_arr, p_md.reshape(1, 345), axis=0)            

            q_df = pd.read_csv(q_filename_csv, encoding='latin1', sep='\t', skiprows=1)
            q_md = q_df.iloc[:,0].values
            qs = q_df.iloc[:,1].values
            #r_df = pd.read_csv(r_filename_csv, encoding='latin1', sep='\t', skiprows=1)

            l = len(pressure_df.iloc[:, 0])

            K_data = np.zeros(shape=(l, 6))
    
            net_input = np.zeros(shape=(6, 1))

            cs = CubicSpline(dq_md, dqs)
            dq_md_interp = cs(p_md)

            cs = CubicSpline(q_md, qs)
            q_md_interp = cs(p_md)

            K = -dq_md_interp*(1/(p_val-pres))

            Khw = -2 * (math.pi * km) / (mi * (Bres / Hres))

            Kad = K / Khw

            X_in = 2*p_md/poco_len

            K_data[:,0] = p_md
            K_data[:,1] = K
            K_data[:,2] = X_in
            K_data[:,3] = Kad
            K_data[:,4] = dq_md_interp
            K_data[:,5] = q_md_interp

            net_input[:, 0] = [Bres, Hres, p_poco, poco_radius, mi, ms]
            #coefs = np.polyfit(K_data[:, 2], K_data[:, 3], 6)  # Calcula os valores de saida e salva tudo em um arquivo CSV
            #coefs = np.polyfit(K_data[:, 0], K_data[:, 3], pol_order)
            coefs = np.polynomial.legendre.legfit(K_data[:,2], K_data[:,3], pol_order)
            net_output_mat = np.append(net_output_mat, coefs.reshape(1, output_len), axis=0)
            k_df = pd.DataFrame(data=K_data, index=range(0, l), columns=['MD', 'K', 'x', 'Kad', 'dQi', 'Qi'])
            input_df = pd.DataFrame(data=net_input, index=range(0, len(net_input)), columns=['net_input'])
            coefs_df = pd.DataFrame(data=coefs, index=range(0, len(coefs)), columns=['coef'])
            k_coefs_df = pd.concat([k_df, input_df, coefs_df], axis=1)
            k_coefs_df.to_csv(k_filename, sep=',', encoding='utf-8', index=False)

    DataX = pd.DataFrame(data=net_input_mat[1:, :], index=range(0, len(net_input_mat)-1), columns=['raio drenagem', 'altura reservatorio', 
        'pressao poco', 'raio poco', 'viscosidade', 'massa especifica'])
    DataY = pd.DataFrame(data=net_output_mat[1:, :], index=range(0, len(net_input_mat)-1), columns=[str(x) for x in range(0, output_len)])

    DatadQval = pd.DataFrame(data=dq_val_arr[1:, :], index=range(0, len(dq_val_arr)-1), columns=[str(x) for x in dq_md_arr[1,:]])
    DataQval = pd.DataFrame(data=q_val_arr[1:, :], index=range(0, len(q_val_arr)-1), columns=[str(x) for x in q_md_arr[1,:]])
    DataPval = pd.DataFrame(data=p_val_arr[1:, :], index=range(0, len(p_val_arr)-1), columns=[str(x) for x in p_md_arr[1,:]])

    DataX.to_csv('datax.csv', sep=',', encoding='utf-8', index=False)
    DataY.to_csv('datay.csv', sep=',', encoding='utf-8', index=False)

    DatadQval.to_csv('datadq.csv', sep=',', encoding='utf-8', index=False)
    DataQval.to_csv('dataq.csv', sep=',', encoding='utf-8', index=False)
    DataPval.to_csv('datap.csv', sep=',', encoding='utf-8', index=False)

    print('Number of preprocessed cases: ' + str(num_cases))
    return net_input_mat[1:, :], net_output_mat[1:, :], input_file_names