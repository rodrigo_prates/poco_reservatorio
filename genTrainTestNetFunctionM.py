# Script function for net train/test
# from keras.models import model_from_json

import pandas as pd
import numpy as np
import os
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Activation
from ann_visualizer.visualize import ann_viz
from keras import optimizers
from keras.layers.normalization import BatchNormalization
from keras.callbacks import EarlyStopping
import matplotlib.pyplot as plt
import statistics
import math
from sklearn.preprocessing import StandardScaler,MinMaxScaler
from sklearn import decomposition
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error


def genTrainTestNetFunctionM(path, net_input_mat, net_output_mat, input_file_names, md_arr, num_epochs, perc_test, plot_train_predictions, 
	plot_test_predictions, plot_train_valid_losses, save_model, km):
	
	model = Sequential()
	model.add(Dense(15, input_dim=6))       # O melhor sao 2 camadas de 1000 neuronios
	model.add(Activation('sigmoid'))
	model.add(Dense(7))
	model.add(Activation('linear'))

	adam=optimizers.Adam(lr=1.0e-010, beta_1=0.8, beta_2=0.899, epsilon=1e-10, decay=0.0)
	model.compile(loss='mse', optimizer='adam')

	X = net_input_mat
	Y = net_output_mat

	X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.33, random_state=42)
	X_train, X_val, Y_train, Y_val = train_test_split(X_train, Y_train, test_size=0.33, random_state=42)

	np.savetxt('X_train.dat',X_train)
	np.savetxt('Y_train.dat',Y_train)
	np.savetxt('X_test.dat',X_test)
	np.savetxt('Y_test.dat',Y_test)
	np.savetxt('X_val.dat',X_val)
	np.savetxt('Y_val.dat',Y_val)

	num_cases = len(X)

	num_test_cases = len(X_test)

	# normalize the dataset
	x_scaler = StandardScaler()
	Xn_train = x_scaler.fit_transform(X_train)
	y_scaler = StandardScaler()
	Yn_train = y_scaler.fit_transform(Y_train)
	Xn_val  = x_scaler.transform(X_val)
	Yn_val  = y_scaler.transform(Y_val)
	Xn_test = x_scaler.transform(X_test)
	Yn_test = y_scaler.transform(Y_test)

	early_stopping_monitor = EarlyStopping(patience=1000, monitor='val_loss', verbose=0, mode='auto')

	history = model.fit(Xn_train, Yn_train, epochs=100000, callbacks=[early_stopping_monitor], validation_data=(Xn_val,Yn_val))

	#Incluir testes!!!

	print('Número total de Casos: ', num_cases)
	print('Número de Casos para teste: ', num_test_cases)

	if save_model:
		# serialize model to JSON
		model_json = model.to_json()
		with open("model.json", "w") as json_file:
			json_file.write(model_json)
		# serialize weights to HDF5
		model.save_weights("model.h5")
		print("Saved model to disk")

	Yn_pred = model.predict(Xn_test) # Calcular as prediçoes sobre os dados de teste
	Y_pred = y_scaler.inverse_transform(Yn_pred)
	np.savetxt("Y_pred.csv", Y_pred, delimiter=",")

	Yn_pred_train = model.predict(Xn_train) # Calcular as prediçoes sobre os dados de treino
	Y_pred_train = x_scaler.inverse_transform(Yn_pred_train)
	np.savetxt("Y_pred_train.csv", Y_pred_train, delimiter=",")	
	# train_case_errors = ((Y_pred_train - Y_train)**2).mean(axis=1)

	# print('test case errors: ', test_case_errors) 
	# print('train cases error: ', train_case_errors)

	emq=mean_squared_error(Y_test, Y_pred, multioutput='raw_values')
	print('emq= '+str(emq))
	np.savetxt('errorMSEt.dat',emq)
    
	erms=np.sqrt(emq)
	print('erms= '+str(erms))

	vc=100*np.divide(erms,np.absolute(np.mean(Y_test)))
	print('vc= '+str(vc)+'%')
	np.savetxt('vc_t.dat',vc)

	if False:
		for i in range(0, num_test_cases):
			predict = Y_pred[i, :]
			pol = np.poly1d(predict)
			print('pol Estimated:')
			print(pol)
			kbarra = pol(md_arr)

			plt.figure()
			plt.plot(md_arr, kbarra, 'r-', label='K(x) Adimensional')
			plt.title('Resistividade de Teste Caso ' + str(i))
			plt.ylabel('Resistividade')
			plt.xlabel('MD')
			plt.legend()
			plt.grid()
		plt.show()

	if False:
		for i in range(0, 4):
			predict = Y_pred_train[i, :]
			pol = np.poly1d(predict)
			kbarra = pol(md_arr)

			plt.figure()
			plt.plot(md_arr, kbarra, 'r-', label='K(x) Adimensional')
			plt.title('Resistividade de treino Caso ' + str(4 + i))
			plt.ylabel('Resistividade')
			plt.xlabel('MD')
			plt.legend()
			plt.grid()
		plt.show()

	if plot_train_valid_losses:
		plt.plot(history.history['loss'])
		plt.plot(history.history['val_loss'])
		plt.title('model loss')
		plt.ylabel('loss')
		plt.xlabel('epoch')
		plt.legend(['Train', 'Validation'], loc='upper right')
		plt.savefig('loss.pdf')
		plt.show()  

	return model, 0, 0, 0, 0
