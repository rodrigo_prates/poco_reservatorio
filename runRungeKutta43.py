# Script function for Runge Kutta 4th order

from pressFlowEquation import pressFlowEquation
import numpy as np
import math

def runRungeKutta43(num_iter, errors, stop_RK4, tol, p, q, k_inv, deltap, deltap_inc, deltap_dec, pres, t, poco_radius, ms, mi, h, n, 
	pheel, iter_count, best_p, best_q, stop_monitor, REs, best_iter, p_old, tol2):

	while iter_count < num_iter:
		iter_count = iter_count + 1
		error1 = abs(p[n-1] - pheel)
		print('iterações: ', iter_count)
		print('Melhor iteração: ', best_iter)

		p_old = np.copy(p)
		q_old = np.copy(q)

		for i in range(0,len(t)-1):
			REi = 0
			fa = 0
			if q[i] != 0:
				REi = (ms*((2/(math.pi*(poco_radius)))*abs(q[i])))/mi
				fa = 0.0791/(REi**0.25)
			
			REs[i] = REi

			m0 = pressFlowEquation(t[i], [p[i], q[i]], fa, k_inv[i], ms, poco_radius, pres)
			m1 = pressFlowEquation(t[i]+0.5*h, [p[i]+0.5*h*m0[0], q[i]+0.5*h*m0[1]], fa, k_inv[i], ms, poco_radius, pres)
			m2 = pressFlowEquation((t[i]+0.5*h), [p[i]+0.5*h*m1[0], q[i]+0.5*h*m1[1]], fa, k_inv[i], ms, poco_radius, pres)
			m3 = pressFlowEquation(t[i]+h, [p[i]+h*m2[0], q[i]+h*m2[1]], fa, k_inv[i], ms, poco_radius, pres)
			p[i+1] = p[i] + (1/6)*( m0[0]+2*m1[0]+2*m2[0]+m3[0] )*h
			q[i+1] = q[i] + (1/6)*( m0[1]+2*m1[1]+2*m2[1]+m3[1] )*h

		errorp = max(abs(p-p_old))
		errorq = max(abs(q-q_old))

		if not errors:
			best_p = p
			best_q = q
		else:
			if min(errors) > errorp:
				best_p = p
				best_q = q
				best_iter = iter_count

		errors.append(errorp)

		print('error1', error1)
		print('error2', errorp)

		if errorp < tol2:
			break
		else:
			deltap = deltap*deltap_inc
			p[0] = pres - deltap

	return best_p, best_q, REs
