# Script function for Runge Kutta 4th order

from pressFlowEquation import pressFlowEquation
import numpy as np
import math

def RK4(t, p, q, ms, poco_radius, mi, REs, h, k_inv, pres):

	for i in range(0,len(t)-1):
			REi = 0
			fa = 0
			if q[i] != 0:
				REi = (ms*((2/(math.pi*(poco_radius)))*abs(q[i])))/mi
				fa = 0.0791/(REi**0.25)
			
			REs[i] = REi

			m0 = pressFlowEquation(t[i], [p[i], q[i]], fa, k_inv[i], ms, poco_radius, pres)
			m1 = pressFlowEquation(t[i]+0.5*h, [p[i]+0.5*h*m0[0], q[i]+0.5*h*m0[1]], fa, k_inv[i], ms, poco_radius, pres)
			m2 = pressFlowEquation((t[i]+0.5*h), [p[i]+0.5*h*m1[0], q[i]+0.5*h*m1[1]], fa, k_inv[i], ms, poco_radius, pres)
			m3 = pressFlowEquation(t[i]+h, [p[i]+h*m2[0], q[i]+h*m2[1]], fa, k_inv[i], ms, poco_radius, pres)
			p[i+1] = p[i] + (1/6)*( m0[0]+2*m1[0]+2*m2[0]+m3[0] )*h
			q[i+1] = q[i] + (1/6)*( m0[1]+2*m1[1]+2*m2[1]+m3[1] )*h

	return p, q, REs