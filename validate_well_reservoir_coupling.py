# Script para avaliação da IA de acoplamento poço-reservatorio
# sudo pip install h5py
# Validar se já existe
# Converter permeabilidade multiplicando o kmedio por 1e-15 antes de passar para as funções
# converter a pressão no reservatorio pres para pascal antes de passar para as funções 

import pandas as pd
import numpy as np
import os
from genInOutDataFunction import genInOutDataFunction
from genTrainTestNetFunction import genTrainTestNetFunction
from genTrainTestNetFunctionM import genTrainTestNetFunctionM
from genPosProcessingChartsFunction import genPosProcessingChartsFunction

path = '.'
pressure_to_pascal = 98066
pres = 225*pressure_to_pascal
poco_len = 400
mDarcy_to_m2 = 1e-15
kh = 1500*mDarcy_to_m2
kv = 450*mDarcy_to_m2
km = (kh + kv + 0)/3
input_len = 6

files = os.listdir(path)
p_files = [file for file in files if file.endswith('P.csv')]
file = p_files[0]
P_df = pd.read_csv(file, encoding='latin1', sep='\t', skiprows=1)
md_arr = P_df['MD (m)'].values
x_arr = 2*md_arr/poco_len

num_epochs = 10000
perc_test = 0.05

pol_order = 6
output_len = pol_order + 1

plot_train_predictions = False
plot_test_predictions = True
plot_train_valid_losses = True

save_model = False
load_model = False
#model_name = 'modelObjetivo.json'
model_name = 'model.json'

standard_scaler = False
turn_to_zero = False

#deltap_inc = 1.01
#deltap_dec = 0.99
deltap_inc = 1.01
deltap_dec = 0.99
num_points = len(md_arr)

param_names = ['raio(m)', 'pressão(kgf/cm2)', 'massa especifica(kg/m3)', 'viscosidade(Pa.s)', 'largura(m)', 'altura(m)']

params_list = ['0.07', '117.0', '900', '0.0015', '500', '60']

# ex: ['simML0115_220_750_0025_700_15_P.csv', 'simML017_220_850_004_800_20_P.csv', 'simML014_117_900_0015_500_60_P.csv', 'simML0115_155_775_004_1100_15_P.csv'] 
#right_file = 'simML020_117_800_006_400_15_K.csv'
right_file = 'simML014_117_900_0015_500_60_K.csv'
#right_file = ''

net_input_mat, net_output_mat, input_file_names = genInOutDataFunction(path, pressure_to_pascal, pres, poco_len, km, input_len, output_len, pol_order)

model, in_data_mean, in_data_max, out_data_mean, out_data_max, in_scaler, out_scaler = genTrainTestNetFunction(path, net_input_mat, net_output_mat, input_file_names, 
	md_arr, num_epochs, perc_test, plot_train_predictions, plot_test_predictions, plot_train_valid_losses, save_model, load_model, 
	model_name, km, standard_scaler, turn_to_zero, input_len, output_len, x_arr)

P, Q = genPosProcessingChartsFunction(path, model, params_list, md_arr, km, poco_len, pres, pressure_to_pascal, load_model, model_name, deltap_inc, 
	deltap_dec, num_points, right_file, in_data_mean, in_data_max, out_data_mean, out_data_max, standard_scaler, in_scaler, out_scaler, turn_to_zero, x_arr)
