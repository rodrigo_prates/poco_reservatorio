# Script function for Runge Kutta 4th order

from pressFlowEquation import pressFlowEquation
import numpy as np
import math
from RK4 import RK4

def runRungeKutta42(num_iter, errors, stop_RK4, tol, p, q, k_inv, deltap, deltap_inc, deltap_dec, pres, t, poco_radius, ms, mi, h, n, 
	pheel, iter_count, best_p, best_q, stop_monitor, REs, best_iter):

	first_q = np.zeros(shape=(len(t)))

	while iter_count < num_iter:
		iter_count = iter_count + 1
		error = abs(p[n-1] - pheel)
		print('iterações: ', iter_count)

		if not errors:
			best_p = p
			best_q = q
		else:
			if min(errors) > error:
				best_p = p
				best_q = q
				best_iter = iter_count
			else:
				stop_monitor += 1

		errors.append(error)

		if stop_monitor >= stop_RK4:
			break 

		if error < tol:
			break
		else:
			if p[n-1] > pheel:
				deltap = deltap*deltap_inc
				p[0] = pres - deltap
			else:
				deltap = deltap*deltap_dec
				p[0] = pres - deltap

		print('melhor iteração: ', best_iter)
		print('menor erro: ', min(errors))

		p, q, REs = RK4(t, p, q, ms, poco_radius, mi, REs, h, k_inv, pres)

		#if iter_count == 3:
		#	first_q = np.copy(q)

	return best_p, q, REs