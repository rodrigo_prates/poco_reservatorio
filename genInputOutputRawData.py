#Script python3 para criação dos arquivos com os dados de entrada para o treinamento da rede neural

import pandas as pd
import numpy as np
import os
import math
import re
import shutil

mDarcy_to_m2 = 1e-15
pressure_to_pascal = 98066
pres = 225*pressure_to_pascal
poco_len = 400
kh = 1500*mDarcy_to_m2
kv = 450*mDarcy_to_m2
path = '/home/rodrigo/Downloads/Dados_Simulacoes_ip3d_rede_neurais_old'

for filename in os.listdir(path): #loop sobre a pasta com os casos em formato xls
    if filename.endswith('P.xls'):
        p_filename_xls = filename
        p_filename_csv = filename.split('P.xls')[0] + 'P.csv'  #Monta os nomes dos arquivos csv a serem salvos com os dados para treinamento
        dq_filename_xls = filename.split('P.xls')[0] + 'dQ.xls'
        dq_filename_csv = filename.split('P.xls')[0] + 'dQ.csv'
        k_filename = filename.split('P.xls')[0] + 'K.csv'
        r_filename_xls = filename.split('P.xls')[0] + 'R.xls'
        r_filename_csv = filename.split('P.xls')[0] + 'R.csv'
        q_filename_xls = filename.split('P.xls')[0] + 'Q.xls'
        q_filename_csv = filename.split('P.xls')[0] + 'Q.csv'

        shutil.copy(p_filename_xls, p_filename_csv)
        shutil.copy(dq_filename_xls, dq_filename_csv)
        shutil.copy(r_filename_xls, r_filename_csv)
        shutil.copy(q_filename_xls, q_filename_csv)

        
        print(p_filename_xls)
        print(dq_filename_xls)
        print(r_filename_xls)
        print(q_filename_xls)

        pressure_df = pd.read_csv(p_filename_csv, encoding='latin1', sep='\t', skiprows=1)
        dq_df = pd.read_csv(dq_filename_csv, encoding='latin1', sep='\t', skiprows=1)
        r_df = pd.read_csv(r_filename_csv, encoding='latin1', sep='\t', skiprows=1)
        
        l = len(pressure_df.iloc[:,0])

        K_data = np.zeros( shape=( l, 4 ) )

        net_input = np.zeros( shape=( 6, 1 ) )

        for idx, P_data in pressure_df.iterrows(): #loop sobre cada arquivo xls, tomando como base o arquivo de pressão
            md = P_data.iloc[0]
            P = P_data.iloc[1]*pressure_to_pascal
            diff = (dq_df['MD (m)']-md).abs() # obtem o valor de dQ a partir do MD
            sorted_diff = diff.argsort()
            dQ = dq_df.iloc[sorted_diff[:1]].iloc[0][1]
            K = -dQ*( 1/( P-pres ) )
            
            k = (kh + kv + 0)/3

            mi = float('0.' + p_filename_csv.split('_')[3])
            Bres = float(p_filename_csv.split('_')[4])
            Hres = float(p_filename_csv.split('_')[5])
            p_poco = float(p_filename_csv.split('_')[1])*pressure_to_pascal
            poco_radius = float('0.' + re.findall('\d+', p_filename_csv)[0][1:])/2
            ms = float(p_filename_csv.split('_')[2])

            Khw = -2*(math.pi*k)/(mi*(Bres/Hres))

            Kad = K/Khw

            x_in = 2*md/poco_len

            K_data[idx,:] = [md, K, x_in, Kad]
            net_input[:,0] = [Bres, Hres, p_poco, poco_radius, mi, ms]

        #coefs = np.polyfit(K_data[:,2], K_data[:,3], 6) # Calcula os valores de saida e salva tudo em um arquivo CSV
        coefs = np.polyfit(K_data[:,0], K_data[:,3], 6)
        k_df = pd.DataFrame( data=K_data, index=range(0, l), columns = ['MD', 'K', 'x', 'Kad'] )
        input_df = pd.DataFrame( data=net_input, index=range(0, len(net_input)), columns = ['net_input'] )
        coefs_df = pd.DataFrame( data=coefs, index=range(0, len(coefs)), columns = ['coef'] )
        k_coefs_df = pd.concat( [k_df, input_df, coefs_df], axis=1 )
        k_coefs_df.to_csv(k_filename, sep=',', encoding='utf-8', index=False)


