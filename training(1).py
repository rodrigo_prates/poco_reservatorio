#!/usr/bin/python

# Tirar o Re = 2400 do treinamento


def main():

    import numpy as np
    from keras.models import Sequential
    from keras.layers import Activation,Dense
    from sklearn.preprocessing import StandardScaler,MinMaxScaler
    from sklearn import decomposition
    from keras.callbacks import EarlyStopping
    from keras import optimizers
    import pandas as pd
    from sklearn.model_selection import train_test_split

    #load dataset
    X = pd.read_csv('datax.csv', encoding='utf-8', sep=',')
    X = X.values

    Y = pd.read_csv('datay.csv', encoding='utf-8', sep=',')
    Y = Y.values

    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=42)
    X_train, X_val, Y_train, Y_val = train_test_split(X_train, Y_train, test_size=0.1, random_state=42)

    np.savetxt('X_train.dat',X_train)
    np.savetxt('Y_train.dat',Y_train)
    np.savetxt('X_test.dat',X_test)
    np.savetxt('Y_test.dat',Y_test)
    np.savetxt('X_val.dat',X_val)
    np.savetxt('Y_val.dat',Y_val)


  
    # normalize the dataset
    x_scaler = StandardScaler()
    Xn_train = x_scaler.fit_transform(X_train)
    y_scaler = StandardScaler()
    Yn_train = y_scaler.fit_transform(Y_train)
    Xn_val  = x_scaler.transform(X_val)
    Yn_val  = y_scaler.transform(Y_val)
    Xn_test = x_scaler.transform(X_test)
    Yn_test = y_scaler.transform(Y_test)   


    # create model
    hidenNeurons=10
    print('hidenNeurons= '+str(hidenNeurons))
    featurings=Xn_train.shape[1]
    print('featurings= '+str(featurings))

    model = Sequential()
    model.add(Dense(hidenNeurons, input_dim=featurings))       # O melhor sao 2 camadas de 100 neuronios
    model.add(Activation('sigmoid'))
#    model.add(Dense(hidenNeurons))      #segunda camada escondida
#    model.add(Activation('sigmoid'))    #segunda camada escondida
    model.add(Dense(Yn_train.shape[1]))
    model.add(Activation('linear'))
                                                           
    # Compile model
    adam=optimizers.Adam(lr=1e-5, beta_1=0.8, beta_2=0.899, epsilon=1e-10, decay=0.0)
    model.compile(loss='mse', optimizer='adam')    


    # Fit the network
    earlyStopping=EarlyStopping(monitor='val_loss', patience=500, verbose=0, mode='auto')   
    history = model.fit(Xn_train, Yn_train, epochs=50000, callbacks=[earlyStopping], validation_data=(Xn_val,Yn_val))
#    history = model.fit(Xn_train, Yn_train, epochs=50000, validation_data=(Xn_val,Yn_val))

    #save
    # serialize model to JSON
    model_json = model.to_json()
    with open("corretores/modelObjetivo.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("corretores/modelObjetivo.h5")
    print("Saved model to disk")


    print(history)
    import matplotlib.pyplot as plt
    plt.semilogy(history.history['loss'],label='Train')
    plt.semilogy(history.history['val_loss'],label='Validation')
    plt.title('model loss')
    plt.ylabel('Loss')
    plt.xlabel('epoch')
    plt.legend(loc='upper right')
    plt.savefig('loss.pdf')   


    # Make predictions
    Yn_pred = model.predict(Xn_test)
    Y_pred = y_scaler.inverse_transform(Yn_pred)
    np.savetxt('Y_pred.dat',Y_pred)




    # Evaluate the network
    from sklearn.metrics import mean_squared_error
    emq=mean_squared_error(Y_test, Y_pred, multioutput='raw_values')
    print('emq= '+str(emq))
    np.savetxt('errorMSEt.dat',emq)
    
    erms=np.sqrt(emq)
    print('erms= '+str(erms))
    print('mean= '+str(np.mean(Y_test,axis=0)))

    vc=100*np.divide(erms,np.absolute(np.mean(Y_test,axis=0)))
    print('vc= '+str(vc)+'%')
    np.savetxt('vc_t.dat',vc)



main()
