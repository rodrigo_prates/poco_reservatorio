# Script function for graph results generation
#                raio do poço(m), pressão no heel(kgf/cm2), massa especifica(), viscosidade do oleo(), largura do reservatorio(m), altura do reservatorio(m)
# params_list = ['0.10', '117', '900', '0.003', '800', '15']
# net_input = [800.0, 15.0, ]
# https://machinelearningmastery.com/save-load-keras-deep-learning-models/

from keras.models import model_from_json
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import statistics
import math
import random
import time
from sklearn.preprocessing import StandardScaler,MinMaxScaler
from runRungeKutta4 import runRungeKutta4
from runRungeKutta42 import runRungeKutta42
from runRungeKutta43 import runRungeKutta43

def genPosProcessingChartsFunction(path, model, params_list, md_arr, km, poco_len, pres, pressure_to_pascal, load_model, model_name, deltap_inc, 
	deltap_dec, num_points, right_file, in_data_mean, in_data_max, out_data_mean, out_data_max, standard_scaler, in_scaler, out_scaler, turn_to_zero, x_arr):

	start = time.clock()

	poco_radius = float(params_list[0])
	p_poco = float(params_list[1])*pressure_to_pascal
	ms = float(params_list[2])
	mi = float(params_list[3]) 	
	Bres = float(params_list[4])
	Hres = float(params_list[5])
	#[Bres, Hres, p_poco, poco_radius, mi, ms]
	net_input = [Bres, Hres, p_poco, poco_radius, mi, ms]
	print('net_input = ', net_input)

	print('num_points')
	print(num_points)

	n = num_points
	#h = -1/2
	t = np.flip(np.arange(n),0)

	p = np.zeros(shape=(len(t)))
	q = np.zeros(shape=(len(t)))

	p_new = np.zeros(shape=(len(t)))
	q_new = np.zeros(shape=(len(t)))

	pheel = p_poco
	deltap = random.uniform(1, 1)*pressure_to_pascal
	ptoe = pres - deltap
	while ptoe < pheel:
		deltap = random.uniform(1, 1)*pressure_to_pascal
		ptoe = pres - deltap
	p[0] = ptoe

	print('deltap')
	print(deltap)

	print('normalization_steps_prediction_single_entry!!')
	in_data = np.asarray(net_input).reshape(1, len(net_input))
	prediction = 0
	if standard_scaler:
		in_data_scaler = StandardScaler()
		in_data_norm = in_data_scaler.fit_transform(in_data)
		prediction = model.predict(in_data_norm)
		#out_scaler = StandardScaler()
		prediction = out_scaler.inverse_transform(prediction)
	else:
		in_data = in_data - in_data_mean
		in_data_norm = in_data / in_data_max
		prediction = model.predict(in_data_norm)
		prediction = prediction*out_data_max + out_data_mean

	if turn_to_zero:
		prediction[:,0] = 0

	#prediction = np.zeros(shape=(1, len(prediction[0,:])))
	#prediction = np.append(prediction, [[-8.482e-14, 1.012e-10, - 4.701e-08, 1.075e-05, -0.001275, 0.07736, -3.198]], axis=0)
	#prediction = prediction[1:, :]

	print('prediction_coefs')
	print(prediction)

	np.savetxt("prediction.csv", prediction, delimiter=",")

	#pol = np.poly1d(prediction[0,:])
	#print('pol')
	#print(pol)

	#kbarra = pol(md_arr)
	kbarra = np.polynomial.legendre.legval(x_arr, prediction[0,:])

	Khw = -2 * (math.pi * km) / (mi * (Bres / Hres))
	print('Khw')
	print(Khw)

	K = kbarra*Khw

	k_inv = np.flip(K, 0)

	tol = 0.00001*pressure_to_pascal
	tol2 = 0.00001
	num_iter = 1000
	iter_count = 0
	stop_RK4 = 500
	stop_monitor = 0
	best_iter = 0
	h = -1

	errors = []
	best_p = np.zeros(shape=(len(t)))
	best_q = np.zeros(shape=(len(t)))
	REs = np.zeros(shape=(len(t)))
	p_old = np.zeros(shape=(len(t)))

	# best_p, best_q, REs = runRungeKutta43(num_iter, errors, stop_RK4, tol, p, q, k_inv, deltap, deltap_inc, deltap_dec, pres, t, 
	# 	poco_radius, ms, mi, h, n, pheel, iter_count, best_p, best_q, stop_monitor, REs, best_iter, p_old, tol2)

	best_p, best_q, REs = runRungeKutta42(num_iter, errors, stop_RK4, tol, p, q, k_inv, deltap, deltap_inc, deltap_dec, pres, t, 
		poco_radius, ms, mi, h, n, pheel, iter_count, best_p, best_q, stop_monitor, REs, best_iter)

	p_poco = np.flip(best_p,0)
	q_poco = np.flip(best_q,0)*-1

	best_p2 = np.zeros(shape=(len(t)))
	best_q2 = np.zeros(shape=(len(t)))
	REs2 = np.zeros(shape=(len(t)))
	k_inv2 = np.zeros(shape=(len(t)))
	p_old2 = np.zeros(shape=(len(t)))
	errors2 = []
	if right_file:
		print("\n")
		print("\n")
		print("\n")
		print("Runge_Kutta para K(x) Real!!")
		K_df = pd.read_csv(right_file, encoding='utf-8', sep=',')
		K2 = K_df['Kad'].values*Khw
		k_inv2 = np.flip(K2, 0)
		iter_count = 0
		stop_monitor = 0
		best_iter = 0
		# best_p2, best_q2, REs2 = runRungeKutta43(num_iter, errors2, stop_RK4, tol, p, q, k_inv2, deltap, deltap_inc, deltap_dec, pres, t, 
		# 	poco_radius, ms, mi, h, n, pheel, iter_count, best_p2, best_q2, stop_monitor, REs2, best_iter, p_old2, tol2)
		best_p2, best_q2, REs2 = runRungeKutta42(num_iter, errors2, stop_RK4, tol, p, q, k_inv2, deltap, deltap_inc, deltap_dec, pres, t, 
			poco_radius, ms, mi, h, n, pheel, iter_count, best_p2, best_q2, stop_monitor, REs2, best_iter)

	p_poco2 = np.flip(best_p2,0)
	q_poco2 = np.flip(best_q2,0)*-1

	print('elipsed Time prediction')
	print(time.clock() - start)

	x = np.arange(len(errors))

	plt.figure()
	dq_poco = -K*(p_poco-pres)
	plt.plot(md_arr, dq_poco, 'r-o', label='dQ/dx estimado')
	if right_file:
		p_filename_csv = right_file.split('K.csv')[0] + 'P.csv'
		P_df = pd.read_csv(p_filename_csv, encoding='latin1', sep='\t', skiprows=1)
		p_orig = P_df.iloc[:,1].values*pressure_to_pascal
		K_df = pd.read_csv(right_file, encoding='utf-8', sep=',')
		kbarra = K_df['Kad'].values
		K_orig = kbarra*Khw
		dq_poco_orig = -K_orig*(p_orig-pres)
		plt.plot(md_arr, dq_poco_orig, 'b-', label='dQ/dx real')

		coefs = K_df['coef'][0:7].values
		kbarra = np.polynomial.legendre.legval(x_arr, coefs)
		K_fit = kbarra*Khw
		dq_poco_fit = -K_fit*(p_poco2-pres)
		plt.plot(md_arr, dq_poco_fit, 'k-', label='dQ/dx de K(x) fitado')

		plt.title('Curva dQ/dx Real VS Estimada')
		plt.xlabel('md')
		plt.ylabel('m3/s/m')
		plt.legend()
		plt.grid()
	else:
		plt.title('Curva dQ/dx')
		plt.xlabel('md')
		plt.ylabel('m3/s/m')
		plt.legend()
		plt.grid()

	plt.figure()
	plt.plot(md_arr,p_poco,'r-o',label='p(x) estimado')
	if right_file:
		p_filename_csv = right_file.split('K.csv')[0] + 'P.csv'
		P_df = pd.read_csv(p_filename_csv, encoding='latin1', sep='\t', skiprows=1)
		p_orig = P_df.iloc[:,1].values*pressure_to_pascal

		diff = p_orig[0] - p_poco[0]

		plt.plot(md_arr,(p_orig - diff),'b-',label='p(x) real')
		plt.plot(md_arr, p_poco2, 'k-', label='p(x) de K(x) real')
		plt.title('Pressão Estimada VS pressão Real')
		plt.xlabel('md')
		plt.ylabel('Pa')
		plt.legend()
		plt.grid()
	else:
		plt.title('Pressão Estimada')
		plt.xlabel('md')
		plt.ylabel('Pa')
		plt.legend()
		plt.grid()

	plt.figure()
	plt.plot(md_arr,q_poco,'r-o',label='q(x) estimado')
	if right_file:
		
		K_df = pd.read_csv(right_file, encoding='utf-8', sep=',')
		Qi = K_df['Qi'].values
		plt.plot(md_arr,Qi,'b-',label='q(x) real')
		plt.plot(md_arr, q_poco2, 'k-', label='q(x) de K(x) real')
		plt.title('Vazão Estimada VS Vazão Real')
		plt.xlabel('md')
		plt.ylabel('m3/s')
		plt.legend()
		plt.grid()
	else:
		plt.title('Vazão estimada')
		plt.xlabel('md')
		plt.ylabel('m3/s')
		plt.legend()
		plt.grid()

	plt.figure()
	plt.plot(x, np.asarray(errors), 'g-', label='e(x)')
	plt.title('Erro Runge Kutta 4 Ordem')
	plt.grid()

	plt.figure()
	plt.plot(md_arr, np.flip(REs,0), 'r-', label='RE(x) Estimado')
	if right_file:
		plt.plot(md_arr, np.flip(REs2,0), 'b-', label='RE(x) Real')
		plt.title('Reynolds estimado vs real')
		plt.grid()
		plt.legend()
	else:
		plt.title('Reynolds estimado')
		plt.grid()
		plt.legend()

	plt.figure()
	plt.plot(md_arr, K, 'r-', label='K(x) Estimado')
	#plt.title('K estimado')
	#plt.grid()
	#plt.legend()

	if right_file:
		#print('Comparing K!!')
		print('polinomio Estimado')
		#print(pol)
		print(prediction[0,:])
		K_df = pd.read_csv(right_file, encoding='utf-8', sep=',')
		coefs = K_df['coef'][0:7].values
		#pol = np.poly1d(coefs)
		print('polinomio Real')
		#print(pol)
		#kbarra = pol(md_arr)
		kbarra = np.polynomial.legendre.legval(x_arr, coefs)
		print(coefs)
		K_orig_fit = kbarra*Khw
		#plt.figure()
		plt.plot(md_arr, K_orig_fit, 'k-', label='K(x) Real Fitado')

		kbarra = K_df['Kad'].values
		K_orig = kbarra*Khw

		plt.plot(md_arr, K_orig, 'b-', label='K(x) Real')

		plt.title('K estimado vs K real')
		plt.grid()
		plt.legend()
	else:
		plt.title('K estimado')
		plt.grid()
		plt.legend()

	plt.show()

	N = len(p_poco)

	#print(N)
	#print(len(p_poco))
	#print(len(q_poco))

	# results = np.zeros( shape=( N, 3 ) )
	results = md_arr.reshape(1,N)
	#print(results)
	results = np.append(results, p_poco.reshape(1,N), axis=0)
	results = np.append(results, q_poco.reshape(1,N), axis=0)
	#print(results)
	result_df = pd.DataFrame( data=results.transpose(), index=range(0, N), columns = ['MD', 'P', 'Q'] )
	result_df.to_csv('results.csv', sep=',', encoding='utf-8', index=False)

	return p_poco, q_poco