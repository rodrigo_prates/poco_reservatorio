# Script function for net train/test
# from keras.models import model_from_json

import pandas as pd
import numpy as np
import os
from keras.models import Sequential
from keras.layers import Dense
from ann_visualizer.visualize import ann_viz
from keras import optimizers
from keras.layers.normalization import BatchNormalization
from keras.callbacks import EarlyStopping
import matplotlib.pyplot as plt
import statistics
import math
from sklearn.preprocessing import StandardScaler
from keras.models import model_from_json

def genTrainTestNetFunction(path, net_input_mat, net_output_mat, input_file_names, md_arr, num_epochs, perc_test, plot_train_predictions, 
	plot_test_predictions, plot_train_valid_losses, save_model, load_model, model_name, km, standard_scaler, turn_to_zero, input_len, output_len, x_arr):

	in_data = net_input_mat
	out_data = net_output_mat

	in_data_mean = 0 
	in_data_max = 0 
	out_data_mean = 0
	out_data_max = 0

	in_scaler = 0
	out_scaler = 0

	if standard_scaler:

		in_scaler = StandardScaler()
		in_data_norm = in_scaler.fit_transform(in_data)

		out_scaler = StandardScaler()
		out_data_norm = out_scaler.fit_transform(out_data)

	else:

		in_data_mean = in_data.mean(axis=0) # Remover a média e normalizar os dados de entrada e saida
		out_data_mean = out_data.mean(axis=0)

		in_data = in_data - in_data_mean
		out_data = out_data - out_data_mean

		in_data_max = np.max(np.abs(in_data), axis=0)
		out_data_max = np.max(np.abs(out_data), axis=0)

		in_data_norm = in_data / in_data_max
		out_data_norm = out_data / out_data_max

	num_cases = len(in_data_norm)

	num_test_cases = round(num_cases*perc_test)

	history = 0
	model = 0

	if load_model:
		# load json and create model
		json_file = open(model_name, 'r')
		model_json = json_file.read()
		json_file.close()
		model = model_from_json(model_json)
		# load weights into new model
		model_name_arr = model_name.split('.json')
		model.load_weights(model_name_arr[0] + '.h5')
		print("Loaded model from disk")
	else:
		model = Sequential() # cria o modelo da rede, o mais parecido com a rede usada na dissertação.
		#model.add(Dense(7, input_dim=6, kernel_initializer='random_uniform', bias_initializer='zeros', activation='tanh'))
		model.add(Dense(10, input_dim=input_len, kernel_initializer='random_uniform', bias_initializer='zeros', activation='sigmoid'))
		model.add(BatchNormalization())
		#model.add(Dense(10, kernel_initializer='random_uniform', bias_initializer='zeros', activation='tanh'))
		model.add(Dense(output_len, kernel_initializer='random_uniform', bias_initializer='zeros', activation='sigmoid')) #Usar funcao linear!!!
		model.add(BatchNormalization())

		sgd = optimizers.SGD(lr=0.005, momentum=0.0000001)
		adam = optimizers.Adam(lr=0.005, beta_1=0.8, beta_2=0.899, epsilon=1e-10, decay=0.0)
		#model.compile(loss='binary_crossentropy', optimizer=adam, metrics=['mse', 'accuracy'])
		model.compile(loss='mean_squared_error', optimizer=adam, metrics=['mse'])

		early_stopping_monitor = EarlyStopping(patience=2000, monitor='val_loss', verbose=0, mode='auto')

		history = model.fit(in_data_norm[num_test_cases:, :], out_data_norm[num_test_cases:, :], validation_split=0.3, nb_epoch=num_epochs, #validation_data=
		verbose=2, callbacks=[early_stopping_monitor]) #aplicar metricas de erro: 

		if save_model:
		# serialize model to JSON
			model_json = model.to_json()
			with open(model_name, "w") as json_file:
				json_file.write(model_json)
		# serialize weights to HDF5
			model_name_arr = model_name.split('.json')
			model.save_weights(model_name_arr[0] + '.h5')
			print("Saved model to disk")

	print('Número total de Casos: ', num_cases)
	print('Número de Casos para teste: ', num_test_cases)

	test_predictions = model.predict(in_data_norm[0:num_test_cases, :]) # Calcular as prediçoes sobre os dados de teste
	train_predictions = model.predict(in_data_norm[num_test_cases:, :]) # Calcular as prediçoes sobre os dados de treinamento

	if standard_scaler:
		test_predictions = out_scaler.inverse_transform(test_predictions)
		train_predictions = out_scaler.inverse_transform(train_predictions)
	else:
		test_predictions = test_predictions*out_data_max + out_data_mean
		train_predictions = train_predictions*out_data_max + out_data_mean

	if turn_to_zero:
		test_predictions[:,0] = 0
		train_predictions[:,0] = 0

	np.savetxt("test_predictions.csv", test_predictions, delimiter=",")
	np.savetxt("train_predictions.csv", train_predictions, delimiter=",")

	test_case_errors = ((test_predictions - out_data[0:num_test_cases, :])**2).mean(axis=1)
	train_case_errors = ((train_predictions - out_data[num_test_cases:, :])**2).mean(axis=1)
	test_case_errors2 = (np.sqrt(((test_predictions - out_data[0:num_test_cases, :])**2).mean(axis=1)))/(np.max(out_data[0:num_test_cases, :]) - 
		np.min(out_data[0:num_test_cases, :]))
	test_case_errors3 = ((np.sum(np.abs((test_predictions - out_data[0:num_test_cases, :])/test_predictions), axis=1))/num_test_cases)
	test_case_errors4 = np.sqrt( ( ( (test_predictions - out_data[0:num_test_cases, :]) / test_predictions )**2).mean(axis=1) )
	print('Normalized Root Mean Square Deviation (NRMSD): ', test_case_errors2)
	print('Mean Absolute Percentage Error (MAPE): ', test_case_errors3)
	print('Root Mean Square Percentage Error (RMSPE): ', test_case_errors4)

	#print('test case errors: ', test_case_errors) 
	#print('train cases error: ', train_case_errors)

	plt.rcParams.update({'font.size': 22})

	if plot_test_predictions:
		for i in range(0, num_test_cases):
			predict = test_predictions[i, :]
			#pol = np.poly1d(predict)
			print('pol Estimated:')
			#print(pol)
			#kbarra = pol(md_arr)
			kbarra = np.polynomial.legendre.legval(x_arr, predict)
			print(predict)

			test_file_name = input_file_names[i]
			mi = float('0.' + test_file_name.split('_')[3])
			Bres = float(test_file_name.split('_')[4])
			Hres = float(test_file_name.split('_')[5])
			Khw = -2 * (math.pi * km) / (mi * (Bres / Hres))
			K = kbarra*Khw

			k_file_name = test_file_name.split('P.csv')[0] + 'K.csv'
			K_df = pd.read_csv(k_file_name, encoding='utf-8', sep=',')
			coefs = K_df['coef'][0:output_len].values
			#pol = np.poly1d(coefs)
			print('pol Real fitado:')
			#print(pol)
			#kbarra = pol(md_arr)
			kbarra = np.polynomial.legendre.legval(x_arr, coefs)
			print(coefs)

			K_orig_fit = kbarra*Khw

			kbarra = K_df['Kad'].values
			K_orig = kbarra*Khw

			plt.figure()
			plt.plot(md_arr, K_orig_fit, 'b-', label='K(x) Real Fitado')
			plt.plot(md_arr, K, 'r-', label='K(x) Estimado')
			plt.plot(md_arr, K_orig, 'k-', label='K(x) Real')
			plt.title('Resistividade de Teste Caso ' + str(i))
			plt.ylabel('Resistividade K(x)')
			plt.xlabel('MD')
			plt.legend()
			plt.grid()
		plt.show()

	if plot_train_predictions:
		for i in range(0, 4):
			predict = train_predictions[i, :]
			#pol = np.poly1d(predict)
			#kbarra = pol(md_arr)
			kbarra = np.polynomial.legendre.legval(x_arr, predict)
			print(predict)

			train_file_name = input_file_names[4 + i]
			mi = float('0.' + train_file_name.split('_')[3])
			Bres = float(train_file_name.split('_')[4])
			Hres = float(train_file_name.split('_')[5])
			Khw = -2 * (math.pi * km) / (mi * (Bres / Hres))
			K = kbarra*Khw

			k_file_name = train_file_name.split('P.csv')[0] + 'K.csv'
			K_df = pd.read_csv(k_file_name, encoding='utf-8', sep=',')
			coefs = K_df['coef'][0:output_len].values
			#pol = np.poly1d(coefs)
			#kbarra = pol(md_arr)
			kbarra = np.polynomial.legendre.legval(x_arr, coefs)
			K_orig_fit = kbarra*Khw

			kbarra = K_df['Kad'].values
			K_orig = kbarra*Khw

			plt.figure()
			plt.plot(md_arr, K_orig_fit, 'b-', label='K(x) Real Fitado')
			plt.plot(md_arr, K, 'r-', label='K(x) Estimado')
			plt.plot(md_arr, K_orig, 'k-', label='K(x) Real')
			plt.title('Resistividade de treino Caso ' + str(4 + i))
			plt.ylabel('Resistividade K(x)')
			plt.xlabel('MD')
			plt.legend()
			plt.grid()
		plt.show()

	if plot_train_valid_losses:
		train_loss_list = history.history['loss']
		valid_loss_list = history.history['val_loss']
		plt.figure()
		plt.plot(history.history['loss'], 'b-')
		plt.plot(history.history['val_loss'], 'r-')
		plt.title('Treinamento da Rede Neural')
		plt.ylabel('loss')
		plt.xlabel('epoch')
		plt.legend(['train', 'test'], loc='upper left')
		plt.show()

	return model, in_data_mean, in_data_max, out_data_mean, out_data_max, in_scaler, out_scaler
